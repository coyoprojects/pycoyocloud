#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2019 Denis Meyer
#
# This file is part of pycoyocloud.
#

"""Tenant dialog"""

import logging
import platform

from PyQt5.QtCore import QThreadPool
from PyQt5.QtGui import QFont
from PyQt5.QtCore import QCoreApplication
from PyQt5.QtWidgets import QDialog, QDesktopWidget, QMenuBar, QAction, QGridLayout, QLabel, QListWidget, QPushButton, QApplication, QMessageBox, QCheckBox, QInputDialog, QLineEdit

from i18n.Translations import translate
from lib.coyo.CoyoTenant import CoyoTenant
from lib.coyo.CoyoOps import CoyoOps
from gui.threads.DeleteTenantThread import DeleteTenantThread
from gui.threads.AcdeacTenantThread import AcdeacTenantThread
from gui.threads.NewLicenseThread import NewLicenseThread
from gui.components.Messages import show_dialog_critical, show_dialog_yes_no

TENANTDIALOG_RETURN_CODE_DELETED = 1000
TENANTDIALOG_RETURN_CODE_ACDEAC = 1001
TENANTDIALOG_RETURN_CODE_NEWLICENSE = 1002


class TenantDialog(QDialog):
    """Tenant dialog GUI"""

    def __init__(self, coyodata, df_tenant):
        """Initializes the tenant dialog

        :param coyodata: The CoyoData
        :param df_tenant: The Tenant dataframe
        """
        super().__init__()

        logging.debug('Initializing TenantDialog')

        self.components = []
        self.disabled_components = []
        self.coyodata = coyodata

        self.df_tenant = df_tenant
        self.coyo_tenant = CoyoTenant(df=self.df_tenant)

        self.threadpool = QThreadPool()
        logging.debug('Multithreading with maximum {} threads.'.format(
            self.threadpool.maxThreadCount()))

    def init_ui(self):
        """Initiates application UI"""
        logging.debug('Initializing TenantDialog GUI')

        self.setWindowTitle(translate('GUI.TENANT.WINDOW.TITLE'))

        self.resize(650, 600)
        self.setModal(True)

        self.font_label_header = QFont()
        self.font_label_header.setBold(True)
        self.font_label_header.setPointSize(15)
        self.font_label = QFont()
        self.font_label.setBold(True)
        self.font_label.setPointSize(12)
        self.font_label_status = QFont()
        self.font_label_status.setBold(True)
        self.font_label_status.setPointSize(12)

        self._center()
        self._init_ui()

    def _init_ui(self):
        """Initializes the dialog"""
        logging.debug('Initializing CreateTenantDialog defaults')

        self.grid = QGridLayout()
        self.grid.setSpacing(10)

        self.label_empty = QLabel('')
        self.label_about = QLabel(translate('GUI.TENANT.LABEL.ABOUT'))
        self.label_status = QLabel('')
        self.label_id = QLabel(translate('GUI.TENANT.LABEL.ID'))
        self.label_active = QLabel(translate('GUI.TENANT.LABEL.ACTIVE'))
        self.label_created = QLabel(translate('GUI.TENANT.LABEL.CREATED'))
        self.label_modified = QLabel(translate('GUI.TENANT.LABEL.MODIFIED'))
        self.label_version = QLabel(translate('GUI.TENANT.LABEL.VERSION'))
        self.label_properties = QLabel(
            translate('GUI.TENANT.LABEL.PROPERTIES'))
        self.label_properties_url = QLabel(
            translate('GUI.TENANT.LABEL.PROPERTIES.URL'))
        self.label_properties_license = QLabel(
            translate('GUI.TENANT.LABEL.PROPERTIES.LICENSE'))
        self.label_authorities = QLabel(
            translate('GUI.TENANT.LABEL.AUTHORITIES'))
        self.label_actions = QLabel(
            translate('GUI.TENANT.LABEL.ACTIONS'))

        self.label_about.setFont(self.font_label_header)
        self.label_properties.setFont(self.font_label_header)
        self.label_authorities.setFont(self.font_label_header)
        self.label_actions.setFont(self.font_label_header)

        self.label_status.setFont(self.font_label_status)

        self.label_id.setFont(self.font_label)
        self.label_active.setFont(self.font_label)
        self.label_created.setFont(self.font_label)
        self.label_modified.setFont(self.font_label)
        self.label_version.setFont(self.font_label)
        self.label_properties_url.setFont(self.font_label)
        self.label_properties_license.setFont(self.font_label)

        self.label_id_val = QLabel('{}'.format(self.coyo_tenant.id))
        self.checkbox_active = QCheckBox(str(self.coyo_tenant.active))
        self.checkbox_active.setChecked(self.coyo_tenant.active)
        self.label_created_val = QLabel('{}'.format(self.coyo_tenant.created))
        self.label_modified_val = QLabel(
            '{}'.format(self.coyo_tenant.modified))
        self.label_version_val = QLabel('{}'.format(self.coyo_tenant.version))
        if self.coyo_tenant.hasUrl():
            url = self.coyo_tenant.properties['url']
            self.label_properties_url_val = QLabel(
                '<a href="{}"><font color="blue">{}</font></a>'.format(url, url))
            self.label_properties_url_val.setOpenExternalLinks(True)
        else:
            self.label_properties_url_val = QLabel(
                'GUI.TENANT.LABEL.PROPERTIES.URL.EMPTY')
        self.label_properties_license_val = QLabel(self._get_license_extract())
        self.list_authorities = QListWidget(self)
        self.list_authorities.addItems(self.coyo_tenant.authorities)

        self.button_copytoclipboard_id = QPushButton(
            translate('GUI.TENANT.BUTTON.COPYTOCLIPBOARD.ID'))
        self.button_copytoclipboard_id.clicked[bool].connect(
            self._copy_to_clipboard_id)
        self.button_properties_set_license = QPushButton(
            translate('GUI.TENANT.BUTTON.PROPERTIES.SET_LICENSE'))
        self.button_properties_set_license.clicked[bool].connect(
            self._set_license)
        self.button_copytoclipboard_license = QPushButton(
            translate('GUI.TENANT.BUTTON.COPYTOCLIPBOARD.LICENSE'))
        self.button_copytoclipboard_license.clicked[bool].connect(
            self._copy_to_clipboard_license)

        self.button_acdeac = QPushButton('')
        self.button_acdeac.clicked[bool].connect(self._acdeac)
        self.button_delete = QPushButton(
            translate('GUI.TENANT.BUTTON.DELETE'))
        self.button_delete.clicked[bool].connect(
            self._delete)

        self.disabled_components.append(self.checkbox_active)

        self.components.append(self.button_copytoclipboard_id)
        self.components.append(self.button_copytoclipboard_license)
        self.components.append(self.button_properties_set_license)
        self.components.append(self.button_acdeac)
        self.components.append(self.button_delete)

        max_row_size = 10
        label_size = 2

        curr_gridid = 1
        self.grid.addWidget(self.label_about, curr_gridid, 0, 1, label_size)
        self.grid.addWidget(self.label_status,
                            curr_gridid, 2, 1, max_row_size - label_size)

        curr_gridid += 1
        self.grid.addWidget(self.label_id, curr_gridid, 0, 1, label_size)
        self.grid.addWidget(self.label_id_val, curr_gridid,
                            label_size, 1, max_row_size - label_size * 2)
        self.grid.addWidget(self.button_copytoclipboard_id,
                            curr_gridid, max_row_size - label_size, 1, label_size)
        curr_gridid += 1
        self.grid.addWidget(self.label_active, curr_gridid, 0, 1, label_size)
        self.grid.addWidget(self.checkbox_active, curr_gridid,
                            label_size, 1, max_row_size - label_size)
        curr_gridid += 1
        self.grid.addWidget(self.label_created, curr_gridid, 0, 1, label_size)
        self.grid.addWidget(self.label_created_val, curr_gridid,
                            label_size, 1, max_row_size - label_size)
        curr_gridid += 1
        self.grid.addWidget(self.label_modified, curr_gridid, 0, 1, label_size)
        self.grid.addWidget(self.label_modified_val, curr_gridid,
                            label_size, 1, max_row_size - label_size)
        curr_gridid += 1
        self.grid.addWidget(self.label_version, curr_gridid, 0, 1, label_size)
        self.grid.addWidget(self.label_version_val, curr_gridid,
                            label_size, 1, max_row_size - label_size)

        curr_gridid += 1
        self.grid.addWidget(self.label_empty, curr_gridid, 0, 1, max_row_size)

        curr_gridid += 1
        self.grid.addWidget(self.label_properties,
                            curr_gridid, 0, 1, max_row_size)
        curr_gridid += 1
        self.grid.addWidget(self.label_properties_url,
                            curr_gridid, 0, 1, label_size)
        self.grid.addWidget(self.label_properties_url_val,
                            curr_gridid, label_size, 1, max_row_size - label_size)
        curr_gridid += 1
        self.grid.addWidget(self.label_properties_license,
                            curr_gridid, 0, 1, label_size)
        self.grid.addWidget(self.label_properties_license_val,
                            curr_gridid, label_size, 1, max_row_size - label_size * 2)
        if self.coyo_tenant.hasLicense():
            self.grid.addWidget(self.button_copytoclipboard_license,
                                curr_gridid, max_row_size - label_size, 1, label_size)
        self.grid.addWidget(self.button_properties_set_license,
                            curr_gridid, max_row_size - label_size * 2, 1, label_size)

        curr_gridid += 1
        self.grid.addWidget(self.label_empty, curr_gridid, 0, 1, max_row_size)

        curr_gridid += 1
        self.grid.addWidget(self.label_authorities,
                            curr_gridid, 0, 1, max_row_size)
        curr_gridid += 1
        self.grid.addWidget(self.list_authorities,
                            curr_gridid, 0, 1, max_row_size)

        curr_gridid += 1
        self.grid.addWidget(self.label_empty, curr_gridid, 0, 1, max_row_size)

        curr_gridid += 1
        self.grid.addWidget(self.label_actions,
                            curr_gridid, 0, 1, max_row_size)
        curr_gridid += 1
        self.grid.addWidget(self.button_acdeac,
                            curr_gridid, 0, 1, max_row_size / 2)
        self.grid.addWidget(self.button_delete,
                            curr_gridid, max_row_size / 2, 1, max_row_size / 2)

        self.setLayout(self.grid)
        self._update_texts()
        self._reset_enabled()

    def _get_license_extract(self):
        """Returns a license extract"""
        if 'license' in self.coyo_tenant.properties:
            license = self.coyo_tenant.properties['license']
            if license:
                if len(license) > 30:
                    return license[:15] + '[...]' + license[-15:]
                elif len(license) > 15:
                    return license[:15] + '[...]'
                else:
                    return license
        return translate('GUI.TENANT.LABEL.PROPERTIES.LICENSE.NO_LICENSE')

    def _update_texts(self):
        """Updates component texts"""
        self.button_acdeac.setText(translate(
            'GUI.TENANT.BUTTON.DEACTIVATE' if self.coyo_tenant.active else 'GUI.TENANT.BUTTON.ACTIVATE'))
        self.checkbox_active.setChecked(self.coyo_tenant.active)
        for comp in self.disabled_components:
            comp.setEnabled(False)

    def _acdeactenant(self):
        """Callback on tenant activation/deactivation"""
        logging.info('Successfully activated/deactivated tenant')
        # self.coyo_tenant.active = not self.coyo_tenant.active
        # self._update_texts()

    def _deletedtenant(self):
        """Callback on tenant delete"""
        logging.info('Successfully deleted tenant')

    def _newlicense(self):
        """Callback on new tenant license"""
        logging.info('Successfully set a new license')

    def _callback_processing_result(self):
        """The processing callback, on result"""
        logging.debug('Callback: Processing result')

        self.coyo_tenant.deleted = True

    def _callback_processing_result_acdeac(self):
        """The processing callback, on result"""
        logging.debug('Callback: Processing result')

    def _callback_processing_result_newlicense(self):
        """The processing callback, on result"""
        logging.debug('Callback: Processing result')

        self.done(TENANTDIALOG_RETURN_CODE_NEWLICENSE)

    def _callback_processing_error(self, ex):
        """The processing callback, on error

        :param ex: The exception
        """
        logging.debug('Callback: Processing error')

        logging.error('Failed to delete: "{}"'.format(ex))
        self._reset_enabled()
        show_dialog_critical(
            translate('GUI.TENANT.ERRORS.ERROR'),
            translate('GUI.TENANT.ERRORS.MSG.ERROR_INFO'),
            translate('GUI.TENANT.ERRORS.MSG.ERROR').format(ex), None)

    def _callback_processing_error_newlicense(self, ex):
        """The processing callback, on error

        :param ex: The exception
        """
        logging.debug('Callback: Processing error')

        logging.error('Failed to delete: "{}"'.format(ex))
        self._reset_enabled()
        show_dialog_critical(
            translate('GUI.TENANT.ERRORS.NEWLICENSE.ERROR'),
            translate('GUI.TENANT.ERRORS.NEWLICENSE.MSG.ERROR_INFO'),
            translate('GUI.TENANT.ERRORS.NEWLICENSE.MSG.ERROR').format(ex), None)

    def _callback_processing_finished(self):
        """The processing callback, on finished"""
        logging.debug('Callback: Processing finished')

        self.label_status.setText('')

        if self.coyo_tenant.deleted:
            self.done(TENANTDIALOG_RETURN_CODE_DELETED)
        else:
            self._reset_enabled()

    def _callback_processing_finished_acdeac(self):
        """The processing callback, on finished"""
        logging.debug('Callback: Processing finished')

        self.label_status.setText('')

        self.done(TENANTDIALOG_RETURN_CODE_ACDEAC)

    def _callback_processing_finished_newlicense(self):
        """The processing callback, on finished"""
        logging.debug('Callback: Processing finished')

        self.label_status.setText('')

    def _acdeac(self):
        """Activates/Deactivates the tenant"""
        logging.debug('Starting processing: Activate/Deactivate')

        self._disable()
        acdeac_tenant = show_dialog_yes_no(
            translate(
                'GUI.TENANT.YESNO.DEACTENANT.TITLE' if self.coyo_tenant.active else 'GUI.TENANT.YESNO.ACTENANT.TITLE'),
            translate(
                'GUI.TENANT.YESNO.MSG.DEACTENANT_INFO' if self.coyo_tenant.active else 'GUI.TENANT.YESNO.MSG.ACTENANT_INFO'),
            translate('GUI.TENANT.YESNO.MSG.DEACTENANT' if self.coyo_tenant.active else 'GUI.TENANT.YESNO.MSG.ACTENANT').format(self.coyo_tenant.getUrl()), None) == QMessageBox.Yes
        if acdeac_tenant:
            logging.info('Activating/Deactivating tenant...')
            try:
                ops = CoyoOps(self.coyodata)
                ops.signals.acdeactenant.connect(self._acdeactenant)
                logging.info(
                    'Deactivating tenant...' if self.coyo_tenant.active else 'Activating tenant...')
                self.label_status.setText(
                    '<font color="#FF0000">{}</font>'.format(translate('GUI.TENANT.LABEL.DEACTIVATING' if self.coyo_tenant.active else 'GUI.TENANT.LABEL.ACTIVATING')))
                thread = AcdeacTenantThread(ops, self.coyo_tenant)
                thread.signals.acdeactenantresult.connect(
                    self._callback_processing_result_acdeac)
                thread.signals.acdeactenanterror.connect(
                    self._callback_processing_error)
                thread.signals.acdeactenantfinished.connect(
                    self._callback_processing_finished_acdeac)
                self.threadpool.start(thread)
            except Exception as ex:
                logging.exception(
                    'Could not start activating/deactivating tenant: "{}"'.format(ex))
                show_dialog_critical(
                    translate('GUI.TENANT.ERRORS.ACDEAC.ERROR'),
                    translate(
                        'GUI.TENANT.ERRORS.ACDEAC.MSG.TENANT_ACDEAC_ERROR_INFO'),
                    translate('GUI.TENANT.ERRORS.ACDEAC.MSG.TENANT_ACDEAC_ERROR').format(ex), None)
                self._reset_enabled()
        else:
            logging.info('Not activating/deactivating tenant...')
            self._reset_enabled()

    def _delete(self):
        """Starts the deletion process"""
        logging.debug('Starting processing: Delete')

        self._disable()
        delete_tenant = show_dialog_yes_no(
            translate('GUI.TENANT.YESNO.DELETETENANT.TITLE'),
            translate('GUI.TENANT.YESNO.MSG.DELETETENANT_INFO'),
            translate('GUI.TENANT.YESNO.MSG.DELETETENANT').format(self.coyo_tenant.getUrl()), None) == QMessageBox.Yes
        if delete_tenant:
            logging.info('Deleting tenant...')
            try:
                ops = CoyoOps(self.coyodata)
                ops.signals.deletedtenant.connect(self._deletedtenant)
                self.label_status.setText(
                    '<font color="#FF0000">{}</font>'.format(translate('GUI.TENANT.LABEL.DELETING')))
                thread = DeleteTenantThread(ops, self.coyo_tenant)
                thread.signals.deletetenantresult.connect(
                    self._callback_processing_result)
                thread.signals.deletetenanterror.connect(
                    self._callback_processing_error)
                thread.signals.deletetenantfinished.connect(
                    self._callback_processing_finished)
                self.threadpool.start(thread)
            except Exception as ex:
                logging.exception(
                    'Could not start deleting tenant: "{}"'.format(ex))
                show_dialog_critical(
                    translate('GUI.TENANT.ERRORS.ERROR'),
                    translate('GUI.TENANT.ERRORS.MSG.TENANT_DELETE_ERROR_INFO'),
                    translate('GUI.TENANT.ERRORS.MSG.TENANT_DELETE_ERROR').format(ex), None)
                self._reset_enabled()
        else:
            logging.info('Not deleting tenant...')
            self._reset_enabled()

    def _set_license(self):
        """Sets the license"""
        license = ''
        if 'license' in self.coyo_tenant.properties:
            license = self.coyo_tenant.properties['license']
        license, ok = QInputDialog.getText(self, translate('GUI.TENANT.OKCANCEL.SETLICENSE.TITLE'), translate(
            'GUI.TENANT.OKCANCEL.SETLICENSE'), QLineEdit.Normal, license)
        if not ok:
            logging.debug('Cancelled the new license dialog')
            return

        logging.debug('New license: "{}"'.format(license))
        logging.debug('Starting processing: New license')

        self._disable()
        try:
            ops = CoyoOps(self.coyodata)
            ops.signals.newlicense.connect(self._newlicense)
            logging.info('Setting a new tenant license...')
            self.label_status.setText(
                '<font color="#FF0000">{}</font>'.format(translate('GUI.TENANT.LABEL.NEWLICENSE')))
            thread = NewLicenseThread(ops, self.coyo_tenant, license)
            thread.signals.newlicenseresult.connect(
                self._callback_processing_result_newlicense)
            thread.signals.newlicenseerror.connect(
                self._callback_processing_error_newlicense)
            thread.signals.newlicensefinished.connect(
                self._callback_processing_finished_newlicense)
            self.threadpool.start(thread)
        except Exception as ex:
            logging.exception(
                'Could not start setting new license: "{}"'.format(ex))
            show_dialog_critical(
                translate('GUI.TENANT.ERRORS.NEWLICENSE.ERROR'),
                translate(
                    'GUI.TENANT.ERRORS.NEWLICENSE.MSG.NEWLICENSE_ERROR_INFO'),
                translate('GUI.TENANT.ERRORS.NEWLICENSE.MSG.NEWLICENSE_ERROR').format(ex), None)
            self._reset_enabled()

    def _reset_enabled(self):
        """Resets all component to initial state"""
        logging.debug('Resetting components to enabled state')

        self._enable()

    def _disable(self):
        """Resets all component to disabled state"""
        logging.debug('Disabling components')

        for comp in self.components:
            comp.setEnabled(False)

    def _enable(self):
        """Resets all component to enabled state"""
        logging.debug('Enabling components')

        for comp in self.components:
            comp.setEnabled(True)

    def _center(self):
        """Centers the dialog on the screen"""
        screen = QDesktopWidget().screenGeometry()
        self.move((screen.width() - self.geometry().width()) / 2,
                  (screen.height() - self.geometry().height()) / 2)

    def _copy_to_clipboard(self, string):
        """Copies the string to clipboard

        :param string: The string to copy to clipboard
        """
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard)
        cb.setText(string, mode=cb.Clipboard)

    def _copy_to_clipboard_id(self):
        """Copies the ID to clipboard"""
        self._copy_to_clipboard(self.coyo_tenant.id)

    def _copy_to_clipboard_license(self):
        """Copies the license to clipboard if present"""
        if 'license' in self.coyo_tenant.properties:
            self._copy_to_clipboard(self.coyo_tenant.properties['license'])
