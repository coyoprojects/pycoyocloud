#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2019 Denis Meyer
#
# This file is part of pycoyocloud.
#

"""Main widget"""

import logging
import os
import json
import csv

from PyQt5.QtCore import QThreadPool
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QWidget, QMessageBox, QDesktopWidget, QGridLayout, QLabel, QLineEdit, QPushButton, QFileDialog, QTableView

from lib.AppConfig import app_conf_get
from i18n.Translations import translate
from lib.coyo.CoyoOps import CoyoOps, clear_tenant_cache
from lib.coyo.CoyoData import CoyoData
from gui.threads.DownloadThread import DownloadThread
from gui.components.PandasModel import PandasModel
from gui.components.TenantDialog import TenantDialog, TENANTDIALOG_RETURN_CODE_DELETED, TENANTDIALOG_RETURN_CODE_ACDEAC, TENANTDIALOG_RETURN_CODE_NEWLICENSE
from gui.components.CreateTenantDialog import CreateTenantDialog, CREATETENANTDIALOG_RETURN_CODE_CREATED
from gui.components.Messages import show_dialog_critical


class MainWidget(QWidget):
    """Main widget GUI"""

    def __init__(self, log, coyodata):
        """Initializes the main widget

        :param log: The (end user) message log
        :param coyodata: The CoyoData
        """
        super().__init__()

        logging.debug('Initializing MainWidget')

        self.log = log
        self.components = []
        self.coyodata = coyodata

        self.data_set = False
        self.filter = ''

        self.threadpool = QThreadPool()
        logging.debug('Multithreading with maximum {} threads.'.format(
            self.threadpool.maxThreadCount()))

        self.df_tenants = None

    def init_ui(self):
        """Initiates application UI"""
        logging.debug('Initializing MainWidget GUI')

        self.font_label_header = QFont()
        self.font_label_header.setBold(True)
        self.font_label_header.setPointSize(15)
        self.font_label = QFont()
        self.font_label.setBold(True)
        self.font_label.setPointSize(12)
        self.font_label_info = QFont()
        self.font_label_info.setPointSize(10)

        self.grid = QGridLayout()
        self.grid.setSpacing(10)

        self.label_empty = QLabel('')

        self.label_details_server = QLabel(
            translate('GUI.MAIN.LABEL.SERVER_DETAILS'))
        self.label_statistics_tenants = QLabel(
            translate('GUI.MAIN.LABEL.STATISTICS'))
        self.label_details_actions = QLabel(
            translate('GUI.MAIN.LABEL.ACTIONS'))
        self.label_details_tenants = QLabel(
            translate('GUI.MAIN.LABEL.TENANTS'))
        self.label_details_tenants_filter = QLabel(
            translate('GUI.MAIN.LABEL.TENANTS.FILTER'))
        self.label_details_tenants_filter_info_1 = QLabel(
            translate('GUI.MAIN.LABEL.TENANTS.FILTER.INFO_1'))
        self.label_details_tenants_filter_info_2 = QLabel(
            translate('GUI.MAIN.LABEL.TENANTS.FILTER.INFO_2'))
        self.label_details_tenants_actions = QLabel(
            translate('GUI.MAIN.LABEL.TENANTS.ACTIONS'))

        self.label_baseurl = QLabel(translate('GUI.MAIN.LABEL.BASE_URL'))
        self.label_username = QLabel(translate('GUI.MAIN.LABEL.USERNAME'))
        self.label_password = QLabel(translate('GUI.MAIN.LABEL.PASSWORD'))
        self.label_statistics_tenants_all_tenants_label = QLabel(
            translate('GUI.MAIN.LABEL.STATISTICS.TENANTS'))
        self.label_statistics_tenants_active_tenants_label = QLabel(
            translate('GUI.MAIN.LABEL.STATISTICS.TENANTS_ACTIVE'))
        self.label_statistics_tenants_inactive_tenants_label = QLabel(
            translate('GUI.MAIN.LABEL.STATISTICS.TENANTS_INACTIVE'))
        self.label_statistics_tenants_all_tenants = QLabel('')
        self.label_statistics_tenants_active_tenants = QLabel('')
        self.label_statistics_tenants_inactive_tenants = QLabel('')
        self._reset_statistics()

        self.label_details_server.setFont(self.font_label_header)
        self.label_statistics_tenants.setFont(self.font_label_header)
        self.label_details_actions.setFont(self.font_label_header)
        self.label_details_tenants.setFont(self.font_label_header)
        self.label_details_tenants_filter.setFont(self.font_label)
        self.label_details_tenants_filter_info_1.setFont(self.font_label_info)
        self.label_details_tenants_filter_info_2.setFont(self.font_label_info)
        self.label_details_tenants_actions.setFont(self.font_label)

        self.edit_baseurl = QLineEdit()
        self.edit_baseurl.setText(self.coyodata.baseurl)
        self.edit_username = QLineEdit()
        self.edit_username.setText(self.coyodata.username)
        self.edit_password = QLineEdit()
        self.edit_password.setText(self.coyodata.password)
        self.edit_filter = QLineEdit()
        self.edit_filter.setText(self.filter)
        self.edit_filter.editingFinished.connect(self._apply_filter)

        self.button_reset = QPushButton(translate('GUI.MAIN.BUTTON.RESET'))
        self.button_reset.clicked[bool].connect(self._reset)
        self.button_clear_cache = QPushButton(
            translate('GUI.MAIN.BUTTON.CLEAR_CACHE'))
        self.button_clear_cache.clicked[bool].connect(
            self._clear_cache)
        self.button_start = QPushButton(
            translate('GUI.MAIN.BUTTON.LOAD_TENANTS'))
        self.button_start.clicked[bool].connect(
            self._start_preparing_processing)
        self.button_new_tenant = QPushButton(
            translate('GUI.MAIN.BUTTON.NEW_TENANT'))
        self.button_new_tenant.clicked[bool].connect(
            self._new_tenant)
        self.button_apply_filter = QPushButton(
            translate('GUI.MAIN.BUTTON.APPLY_FILTER'))
        self.button_apply_filter.clicked[bool].connect(
            self._apply_filter)
        self.button_reset_filter = QPushButton(
            translate('GUI.MAIN.BUTTON.RESET_FILTER'))
        self.button_reset_filter.clicked[bool].connect(
            self._reset_filter)

        self.components.append(self.edit_baseurl)
        self.components.append(self.edit_username)
        self.components.append(self.edit_password)
        self.components.append(self.edit_filter)
        self.components.append(self.button_reset)
        self.components.append(self.button_clear_cache)
        self.components.append(self.button_start)
        self.components.append(self.button_new_tenant)
        self.components.append(self.button_apply_filter)
        self.components.append(self.button_reset_filter)

        max_rows = 10

        curr_gridid = 1
        self.grid.addWidget(self.label_details_server, curr_gridid, 0, 1, 5)
        self.grid.addWidget(self.label_details_actions, curr_gridid, 6, 1, 2)
        self.grid.addWidget(self.label_statistics_tenants,
                            curr_gridid, 8, 1, 2)

        curr_gridid += 1
        self.grid.addWidget(self.label_baseurl, curr_gridid, 0, 1, 1)
        self.grid.addWidget(self.edit_baseurl, curr_gridid, 1, 1, 5)
        self.grid.addWidget(self.button_reset, curr_gridid, 6, 1, 2)
        self.grid.addWidget(
            self.label_statistics_tenants_all_tenants_label, curr_gridid, 8, 1, 1)
        self.grid.addWidget(
            self.label_statistics_tenants_all_tenants, curr_gridid, 9, 1, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_username, curr_gridid, 0, 1, 1)
        self.grid.addWidget(self.edit_username, curr_gridid, 1, 1, 5)
        self.grid.addWidget(self.button_clear_cache, curr_gridid, 6, 1, 2)
        self.grid.addWidget(
            self.label_statistics_tenants_active_tenants_label, curr_gridid, 8, 1, 1)
        self.grid.addWidget(
            self.label_statistics_tenants_active_tenants, curr_gridid, 9, 1, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_password, curr_gridid, 0, 1, 1)
        self.grid.addWidget(self.edit_password, curr_gridid, 1, 1, 5)
        self.grid.addWidget(self.button_start, curr_gridid, 6, 1, 2)
        self.grid.addWidget(
            self.label_statistics_tenants_inactive_tenants_label, curr_gridid, 8, 1, 1)
        self.grid.addWidget(
            self.label_statistics_tenants_inactive_tenants, curr_gridid, 9, 1, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_empty, curr_gridid, 0, 1, max_rows)

        curr_gridid += 1
        self.grid.addWidget(self.label_details_tenants,
                            curr_gridid, 0, 1, max_rows)
        curr_gridid += 1
        self.table_model = PandasModel(self)
        self.table_view = QTableView()
        self.table_view.setModel(self.table_model)
        self.table_view.setSortingEnabled(True)
        self.table_view.doubleClicked.connect(self._row_double_clicked)
        self.table_view.setSelectionBehavior(QTableView.SelectRows)
        self.grid.addWidget(self.table_view, curr_gridid, 0, 20, max_rows - 2)
        self.grid.addWidget(self.label_details_tenants_filter,
                            curr_gridid, max_rows - 2, 1, 2)
        curr_gridid += 1
        self.grid.addWidget(self.edit_filter,
                            curr_gridid, max_rows - 2, 1, 2)
        curr_gridid += 1
        self.grid.addWidget(self.button_apply_filter,
                            curr_gridid, max_rows - 2, 1, 2)
        curr_gridid += 1
        self.grid.addWidget(self.button_reset_filter,
                            curr_gridid, max_rows - 2, 1, 2)
        curr_gridid += 1
        self.grid.addWidget(self.label_details_tenants_filter_info_1,
                            curr_gridid, max_rows - 2, 1, 2)
        curr_gridid += 1
        self.grid.addWidget(self.label_details_tenants_filter_info_2,
                            curr_gridid, max_rows - 2, 1, 2)

        curr_gridid += 1
        curr_gridid += 1
        self.grid.addWidget(self.label_details_tenants_actions,
                            curr_gridid, max_rows - 2, 1, 2)
        curr_gridid += 1
        self.grid.addWidget(self.button_new_tenant,
                            curr_gridid, max_rows - 2, 1, 2)

        self.setLayout(self.grid)
        self._reset_enabled()
        self.button_new_tenant.setEnabled(False)

    def _reset_filter(self):
        """Resets the tenant filter"""
        logging.debug('Resetting filter')
        self.edit_filter.setText('')
        self._apply_filter()

    def _apply_filter(self):
        """Applies the tenant filter"""
        logging.debug('Applying filter')
        if self.data_set:
            old_filter = self.filter
            self.filter = self.edit_filter.text()
            if old_filter != self.filter:
                logging.debug('Filter: "{}"'.format(self.filter))
                self.table_model.filter(self.filter)
                self._update_statistics(self.table_model.currentData())

    def _new_tenant(self):
        """Open create new tenant dialog"""
        logging.debug('Resetting filter')
        self._disable()
        self.createtenant_dialog = CreateTenantDialog(self.coyodata)
        self.createtenant_dialog.init_ui()
        self._createtenant_dialog_done(self.createtenant_dialog.exec_())

    def _createtenant_dialog_done(self, code):
        """On create tenant dialog done
        :param code: The return code
        """
        logging.debug('CreateTenantDialog return code: {}'.format(code))
        if code == CREATETENANTDIALOG_RETURN_CODE_CREATED:
            self.log(translate('GUI.MAIN.LOG.TENANT_CREATED'))
            # self._reset()
            self._clear_cache()
            self._start_preparing_processing()
        else:
            self._reset_enabled()

    def _tenant_dialog_done(self, code):
        """On tenant dialog done
        :param code: The return code
        """
        logging.debug('TenantDialog return code: {}'.format(code))
        if code == TENANTDIALOG_RETURN_CODE_DELETED:
            self.log(translate('GUI.MAIN.LOG.TENANT_DELETED'))
            # self._reset()
            self._clear_cache()
            self._start_preparing_processing()
        elif code == TENANTDIALOG_RETURN_CODE_ACDEAC:
            self.log(translate('GUI.MAIN.LOG.TENANT_ACDEAC'))
            # self._reset()
            self._clear_cache()
            self._start_preparing_processing()
        elif code == TENANTDIALOG_RETURN_CODE_NEWLICENSE:
            self.log(translate('GUI.MAIN.LOG.TENANT_NEWLICENSE'))
            # self._reset()
            self._clear_cache()
            self._start_preparing_processing()
        else:
            self._reset_enabled()

    def _row_double_clicked(self, idx_obj):
        """On QTableView row double click

        :param idx_obj: The index object
        """
        if self.is_enabled:
            self._disable()
            logging.debug('Double-clicked row: {}'.format(idx_obj.row()))
            self.tenant_dialog = TenantDialog(
                self.coyodata, self.table_view.model().getValue(idx_obj.row()))
            self.tenant_dialog.init_ui()
            self._tenant_dialog_done(self.tenant_dialog.exec_())

    def _reset_enabled(self):
        """Resets all component to initial state"""
        logging.debug('Resetting components to enabled state')

        self._enable()
        self.df_tenants = None

    def _disable(self):
        """Resets all component to disabled state"""
        logging.debug('Disabling components')

        self.is_enabled = False
        for comp in self.components:
            comp.setEnabled(False)

    def _enable(self):
        """Resets all component to enabled state"""
        logging.debug('Enabling components')

        for comp in self.components:
            comp.setEnabled(True)
        self.is_enabled = True

    def _callback_processing_result(self, df_tenants, from_cache):
        """The processing callback, on result

        :param df_tenants: The tenants dataframe
        """
        logging.debug('Callback: Processing result')

        self.df_tenants = df_tenants

        str = translate('GUI.MAIN.LOG.LOADED') if from_cache else translate(
            'GUI.MAIN.LOG.DOWNLOADED')
        self.log(str.format(len(df_tenants.index)))
        logging.debug(str.format(len(df_tenants.index)))

        self.table_model.setData(self.df_tenants)
        self.data_set = True
        self._update_statistics(self.df_tenants)
        self.button_new_tenant.setEnabled(True)

    def _update_statistics(self, df):
        """Updates the statistics UI

        :param df: The Dataframe
        """
        logging.debug('Updating statistics')
        if self.data_set:
            nr_tenants = self._get_nr_of_rows(df)
            nr_tenants_active = self._get_nr_of_rows(
                df.loc[df['active'] == True])
            nr_tenants_inactive = self._get_nr_of_rows(
                df.loc[df['active'] == False])

            logging.debug('')
            logging.debug('Stats')
            logging.debug('Number of tenants: {}'.format(nr_tenants))
            logging.debug(
                'Number of active tenants: {}'.format(nr_tenants_active))
            logging.debug('Number of inactive tenants: {}'.format(
                nr_tenants_inactive))
            logging.debug('')

            self.label_statistics_tenants_all_tenants.setText(
                '{}'.format(nr_tenants))
            self.label_statistics_tenants_active_tenants.setText(
                '{}'.format(nr_tenants_active))
            self.label_statistics_tenants_inactive_tenants.setText(
                '{}'.format(nr_tenants_inactive))

    def _get_nr_of_rows(self, df):
        """Returns the number of rows of the given Dataframe

        :param df: The Dataframe
        """
        return len(df.index) if self.data_set else -1

    def _callback_processing_error(self, ex):
        """The processing callback, on error

        :param ex: The exception
        """
        logging.debug('Callback: Processing error')

        logging.error('Failed to download: "{}"'.format(ex))
        self.log(translate('GUI.MAIN.LOG.DOWNLOAD_FAILED'))
        self._reset_enabled()
        show_dialog_critical(
            translate('GUI.MAIN.ERRORS.ERROR'),
            translate('GUI.MAIN.ERRORS.MSG.DOWNLOAD_TENANTS_FAILED_INFO'),
            translate('GUI.MAIN.ERRORS.MSG.DOWNLOAD_TENANTS_FAILED').format(ex), None)

    def _callback_processing_finished(self):
        """The processing callback, on finished"""
        logging.debug('Callback: Processing finished')

        self._reset_enabled()

    def _fetchedtenants(self):
        """Callback for tenants fetched"""
        logging.info('Fetching tenants')
        self.log(translate('GUI.MAIN.LOG.FETCHING_TENANTS'))

    def _start_processing(self):
        """Starts the processing"""
        logging.debug('Starting processing')

        self._reset_filter()
        try:
            ops = CoyoOps(self.coyodata)
            ops.signals.fetchedtenants.connect(self._fetchedtenants)
            logging.info('Downloading tenants...')
            self.log(translate('GUI.MAIN.LOG.DOWNLOADING_TENANTS'))
            thread = DownloadThread(ops)
            thread.signals.downloadresult.connect(
                self._callback_processing_result)
            thread.signals.downloaderror.connect(
                self._callback_processing_error)
            thread.signals.downloadfinished.connect(
                self._callback_processing_finished)
            self.threadpool.start(thread)
        except Exception as ex:
            logging.exception('Could not start downloading: "{}"'.format(ex))
            self.log(translate('GUI.MAIN.LOG.DOWNLOADING_TENANTS_ERROR'))
            self.show_dialog_critical(
                translate('GUI.MAIN.ERRORS.ERROR'),
                translate('GUI.MAIN.ERRORS.MSG.DOWNLOAD_FAILED_INFO'),
                translate('GUI.MAIN.ERRORS.MSG.DOWNLOAD_FAILED').format(ex), None)
            self._reset_enabled()

    def _reset(self):
        """Resets everything"""
        logging.debug('Resetting')
        self.log(translate('GUI.MAIN.LOG.RESETTING'))
        self._disable()
        self._reset_filter()
        self._reset_statistics()
        self.df_tenants = None
        self.table_model.reset()
        self._reset_enabled()
        self.data_set = False
        self.log(translate('GUI.MAIN.LOG.RESET_SUCCESS'))
        self.button_new_tenant.setEnabled(False)

    def _reset_statistics(self):
        """Resets the statistics labels"""
        self.label_statistics_tenants_all_tenants.setText(
            translate('GUI.MAIN.VALUE.TENANTS'))
        self.label_statistics_tenants_active_tenants.setText(
            translate('GUI.MAIN.VALUE.TENANTS_ACTIVE'))
        self.label_statistics_tenants_inactive_tenants.setText(
            translate('GUI.MAIN.VALUE.TENANTS_INACTIVE'))

    def _clear_cache(self):
        """Clears the tenant cache"""
        logging.debug('Clearing the cache')
        self.log(translate('GUI.MAIN.LOG.CLEAR_CACHE'))
        clear_tenant_cache()
        self.log(translate('GUI.MAIN.LOG.CLEAR_CACHE_SUCCESS'))

    def _start_preparing_processing(self):
        """Starts the processing process"""
        logging.debug('Starting preparing processing')

        try:
            self._disable()
            errors = []
            baseurl = self.edit_baseurl.text()
            if not baseurl:
                logging.error('Invalid base URL')
                errors.append(translate('GUI.MAIN.ERRORS.BASE_URL'))
            username = self.edit_username.text()
            if not username:
                logging.error('Invalid username')
                errors.append(translate('GUI.MAIN.ERRORS.USERNAME'))
            password = self.edit_password.text()
            if not password:
                logging.error('Invalid password')
                errors.append(translate('GUI.MAIN.ERRORS.USERNAME'))
            if errors:
                err_str = ''
                for i, err in enumerate(errors):
                    if i > 0:
                        err_str += '\n'
                    err_str += '- ' + err
                self.show_dialog_critical(
                    translate('GUI.MAIN.ERRORS.ERROR'),
                    translate('GUI.MAIN.ERRORS.MSG.PARAMS_MISSING_INFO'),
                    translate('GUI.MAIN.ERRORS.MSG.PARAMS_MISSING'), err_str)
                self._reset_enabled()
            else:
                self.coyodata = CoyoData(baseurl=baseurl,
                                         username=username,
                                         password=password)
                try:
                    logging.debug('Saving settings to file')
                    try:
                        with open(app_conf_get('settings_file'), 'r+') as f:
                            settings = json.load(f)
                    except Exception:
                        logging.debug('Settings file does not exist')
                        settings = {}
                    with open(app_conf_get('settings_file'), 'w+') as f:
                        settings['BASE_URL'] = baseurl
                        settings['USERNAME'] = username
                        settings['PASSWORD'] = password
                        json.dump(settings, f)
                        logging.debug('Successfully saved settings to file')
                except Exception as e:
                    logging.error('Error saving settings: "{}"'.format(e))
                self._start_processing()
        except Exception as ex:
            logging.exception('Could not start downloading: "{}"'.format(ex))
            self.log(translate('GUI.MAIN.LOG.DOWNLOADING_TENANTS_ERROR'))
            self.show_dialog_critical(
                translate('GUI.MAIN.ERRORS.ERROR'),
                translate('GUI.MAIN.ERRORS.MSG.DOWNLOAD_FAILED_INFO'),
                translate('GUI.MAIN.ERRORS.MSG.DOWNLOAD_FAILED').format(ex), err_str)
            self._reset_enabled()
