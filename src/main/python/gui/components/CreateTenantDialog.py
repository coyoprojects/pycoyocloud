#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2019 Denis Meyer
#
# This file is part of pycoyocloud.
#

"""Create tenant window"""

import logging
import platform

from PyQt5.QtCore import QThreadPool
from PyQt5.QtGui import QFont
from PyQt5.QtCore import QCoreApplication
from PyQt5.QtWidgets import QDialog, QDesktopWidget, QMenuBar, QAction, QGridLayout, QLabel, QListWidget, QPushButton, QApplication, QMessageBox, QCheckBox, QLineEdit

from i18n.Translations import translate
from lib.coyo.CoyoTenant import CoyoTenant
from lib.coyo.CoyoOps import CoyoOps
from gui.threads.CreateTenantThread import CreateTenantThread
from gui.components.Messages import show_dialog_critical

CREATETENANTDIALOG_RETURN_CODE_CREATED = 2000
CREATETENANTDIALOG_RETURN_CODE_CANCEL = 3000


class CreateTenantDialog(QDialog):
    """Create tenant dialog GUI"""

    def __init__(self, coyodata):
        """Initializes the create tenant dialog

        :param coyodata: The CoyoData
        """
        super().__init__()

        logging.debug('Initializing TenantDialog')

        self.components = []
        self.coyodata = coyodata

        self.threadpool = QThreadPool()
        logging.debug('Multithreading with maximum {} threads.'.format(
            self.threadpool.maxThreadCount()))

    def init_ui(self):
        """Initiates application UI"""
        logging.debug('Initializing CreateTenantDialog GUI')

        self.setWindowTitle(translate('GUI.TENANT.CREATE.WINDOW.TITLE'))

        self.resize(400, 250)
        self.setModal(True)

        self.font_label_header = QFont()
        self.font_label_header.setBold(True)
        self.font_label_header.setPointSize(15)
        self.font_label = QFont()
        self.font_label.setBold(True)
        self.font_label.setPointSize(12)
        self.font_label_creating = QFont()
        self.font_label_creating.setBold(True)
        self.font_label_creating.setPointSize(12)

        self._center()
        self._init_ui()

    def _init_ui(self):
        """Initializes the dialog"""
        logging.debug('Initializing CreateTenantDialog defaults')

        self.grid = QGridLayout()
        self.grid.setSpacing(10)

        self.label_empty = QLabel('')
        self.label_about = QLabel(translate('GUI.TENANT.CREATE.LABEL.ABOUT'))
        self.label_creating = QLabel('')
        self.label_properties = QLabel(
            translate('GUI.TENANT.CREATE.LABEL.PROPERTIES'))
        self.label_active = QLabel(translate('GUI.TENANT.CREATE.LABEL.ACTIVE'))
        self.label_blank = QLabel(translate('GUI.TENANT.CREATE.LABEL.BLANK'))
        self.label_url = QLabel(translate('GUI.TENANT.CREATE.LABEL.URL'))

        self.label_about.setFont(self.font_label_header)
        self.label_properties.setFont(self.font_label_header)

        self.label_creating.setFont(self.font_label_creating)

        self.checkbox_active = QCheckBox()
        self.checkbox_active.setChecked(True)
        self.checkbox_blank = QCheckBox()
        self.checkbox_blank.setChecked(True)
        self.edit_url = QLineEdit()
        self.edit_url.setText('')
        self.label_base_url = QLabel(
            '.{}'.format(self.coyodata.extract_base_url()))

        self.button_create = QPushButton(
            translate('GUI.TENANT.CREATE.BUTTON.CREATE'))
        self.button_create.clicked[bool].connect(
            self._create)
        self.button_cancel = QPushButton(
            translate('GUI.TENANT.CREATE.BUTTON.CANCEL'))
        self.button_cancel.clicked[bool].connect(
            self._cancel)

        self.components.append(self.button_create)
        self.components.append(self.button_cancel)

        max_row_size = 10
        label_size = 1

        curr_gridid = 1
        self.grid.addWidget(self.label_about, curr_gridid, 0, 1, max_row_size)

        curr_gridid += 1
        self.grid.addWidget(self.label_creating,
                            curr_gridid, 0, 1, max_row_size)

        curr_gridid += 1
        self.grid.addWidget(self.label_properties,
                            curr_gridid, 0, 1, max_row_size)

        curr_gridid += 1
        self.grid.addWidget(self.label_active, curr_gridid,
                            0, 1, label_size)
        self.grid.addWidget(self.checkbox_active, curr_gridid,
                            label_size, 1, max_row_size - label_size)

        curr_gridid += 1
        self.grid.addWidget(self.label_blank, curr_gridid, 0, 1, label_size)
        self.grid.addWidget(self.checkbox_blank, curr_gridid,
                            label_size, 1, max_row_size - label_size)

        curr_gridid += 1
        self.grid.addWidget(self.label_url, curr_gridid,
                            0, 1, label_size)
        self.grid.addWidget(self.edit_url, curr_gridid,
                            label_size, 1, max_row_size - label_size * 2)
        self.grid.addWidget(self.label_base_url, curr_gridid,
                            max_row_size - label_size, 1, label_size)

        curr_gridid += 1
        self.grid.addWidget(self.label_empty, curr_gridid, 0, 1, max_row_size)

        curr_gridid += 1
        self.grid.addWidget(self.button_create,
                            curr_gridid, max_row_size / 2, 1, max_row_size / 2)
        self.grid.addWidget(self.button_cancel,
                            curr_gridid, 0, 1, max_row_size / 2)

        self.setLayout(self.grid)
        self._reset_enabled()

    def _clean(self, s):
        """Cleans the given string

        :param s: The string to clean
        """
        new_s = s.replace('ü', 'ue').replace('ä', 'ae').replace('ö', 'oe').replace(
            'Ü', 'Ue').replace('Ä', 'Ae').replace('Ö', 'Oe').replace('ß', 'ss')
        return ''.join([c if c.isalnum() else '' for c in new_s])

    def _cancel(self):
        """Cancels and closes the dialog"""
        self.done(CREATETENANTDIALOG_RETURN_CODE_CANCEL)

    def _createdtenant(self):
        """Callback on tenant create"""
        logging.info('Successfully created tenant')

    def _callback_processing_result(self):
        """The processing callback, on result"""
        logging.debug('Callback: Processing result')

        self.done(CREATETENANTDIALOG_RETURN_CODE_CREATED)

    def _callback_processing_error(self, ex):
        """The processing callback, on error

        :param ex: The exception
        """
        logging.debug('Callback: Processing error')

        logging.error('Failed to create: "{}"'.format(ex))
        show_dialog_critical(
            translate('GUI.TENANT.CREATE.ERRORS.ERROR'),
            translate('GUI.TENANT.CREATE.ERRORS.MSG.TENANT_CREATE_ERROR_INFO'),
            translate('GUI.TENANT.CREATE.ERRORS.MSG.TENANT_CREATE_ERROR').format(ex), None)
        self._reset_enabled()

    def _callback_processing_finished(self):
        """The processing callback, on finished"""
        logging.debug('Callback: Processing finished')

        self.label_creating.setText('')

    def _create(self):
        """Starts the creation process"""
        logging.debug('Starting processing')

        self._disable()
        active = self.checkbox_active.isChecked()
        blank = self.checkbox_blank.isChecked()
        url = self._clean(self.edit_url.text().strip())
        cloud_url = '{}://{}.{}'.format(self.coyodata.extract_protocol(),
                                        url, self.coyodata.extract_base_url())
        if not url:
            logging.debug('Tenant name is blank')

            show_dialog_critical(
                translate('GUI.TENANT.CREATE.ERRORS.URL_BLANK.ERROR'),
                translate(
                    'GUI.TENANT.CREATE.ERRORS.URL_BLANK.MSG.TENANT_CREATE_ERROR_INFO'),
                translate('GUI.TENANT.CREATE.ERRORS.URL_BLANK.MSG.TENANT_CREATE_ERROR'), None)
            self._reset_enabled()
            return
        logging.debug('Active: {}, Blank: {}, URL: "{}", Cloud URL: "{}"'.format(
            active, blank, url, cloud_url))

        self.label_creating.setText(
            '<font color="#FF0000">{}</font>'.format(translate('GUI.TENANT.CREATE.LABEL.CREATING')))
        try:
            ops = CoyoOps(self.coyodata)
            ops.signals.createdtenant.connect(self._createdtenant)
            logging.info('Creating tenant...')

            thread = CreateTenantThread(ops, active, blank, cloud_url)
            thread.signals.createtenantresult.connect(
                self._callback_processing_result)
            thread.signals.createtenanterror.connect(
                self._callback_processing_error)
            thread.signals.createtenantfinished.connect(
                self._callback_processing_finished)
            self.threadpool.start(thread)
        except Exception as ex:
            logging.exception(
                'Could not start creating tenant: "{}"'.format(ex))
            show_dialog_critical(
                translate('GUI.TENANT.CREATE.ERRORS.ERROR'),
                translate(
                    'GUI.TENANT.CREATE.ERRORS.MSG.TENANT_CREATE_ERROR_INFO'),
                translate('GUI.TENANT.CREATE.ERRORS.MSG.TENANT_CREATE_ERROR').format(ex), None)
            self._reset_enabled()

    def _reset_enabled(self):
        """Resets all component to initial state"""
        logging.debug('Resetting components to enabled state')

        self._enable()

    def _disable(self):
        """Resets all component to disabled state"""
        logging.debug('Disabling components')

        for comp in self.components:
            comp.setEnabled(False)

    def _enable(self):
        """Resets all component to enabled state"""
        logging.debug('Enabling components')

        for comp in self.components:
            comp.setEnabled(True)

    def _center(self):
        """Centers the dialog on the screen"""
        screen = QDesktopWidget().screenGeometry()
        self.move((screen.width() - self.geometry().width()) / 2,
                  (screen.height() - self.geometry().height()) / 2)
