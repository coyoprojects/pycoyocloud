#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2019 Denis Meyer
#
# This file is part of pycoyocloud.
#

"""GUI messages"""

from PyQt5.QtWidgets import QMessageBox


def show_dialog_critical(window_title, text, inf_text, detail_text):
    """Shows a dialog, critical

    :param window_title: The window title
    :param text: The text
    :param inf_text: The information text
    :param detail_text: The detail text
    """
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Critical)
    msg.setStandardButtons(QMessageBox.Ok)

    msg.setWindowTitle(window_title)
    msg.setText(text)
    msg.setInformativeText(inf_text)
    msg.setDetailedText(detail_text)

    return msg.exec_()


def show_dialog_yes_no(window_title, text, inf_text, detail_text):
    """Shows a dialog, yes-no

    :param window_title: The window title
    :param text: The text
    :param inf_text: The information text
    :param detail_text: The detail text
    """
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Question)
    msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)

    msg.setWindowTitle(window_title)
    msg.setText(text)
    msg.setInformativeText(inf_text)
    msg.setDetailedText(detail_text)

    return msg.exec_()
