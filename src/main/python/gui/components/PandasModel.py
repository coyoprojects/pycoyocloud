#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2019 Denis Meyer
#
# This file is part of pycoyocloud.
#

"""Main widget"""

import logging
import re

from PyQt5.QtCore import QAbstractTableModel, Qt


class PandasModel(QAbstractTableModel):
    """
    Class to populate a table view with a pandas dataframe
    """

    def __init__(self, parent=None):
        """Initializes the model

        :param parent: The parent
        """
        QAbstractTableModel.__init__(self, parent)
        self._data = None
        self._is_data_set = False

        self._original_data = None

    def setData(self, data, update_orig_data=True):
        """Sets data and updates the view

        :param data: The data
        """
        logging.debug('Setting data')
        self._data = data
        if update_orig_data:
            self._original_data = data
        self._is_data_set = True
        logging.debug('Updating data')
        self.layoutChanged.emit()

    def reset(self):
        """Resets to None"""
        logging.debug('Resetting')
        self._data = None
        self._original_data = None
        self._is_data_set = False
        logging.debug('Updating data')
        self.layoutChanged.emit()

    def rowCount(self, parent=None):
        """Returns the row count

        :param parent: The parent
        """
        return len(self._data.values) if self._is_data_set else 0

    def columnCount(self, parent=None):
        """Returns the column count

        :param parent: The parent
        """
        return self._data.columns.size if self._is_data_set else 0

    def currentData(self):
        """Returns the current data"""
        return self._data

    def data(self, index, role=Qt.DisplayRole):
        """Returns the data

        :param index: The index
        :param role: The role
        """
        if self._is_data_set and index.isValid():
            if role == Qt.DisplayRole:
                return str(self._data.values[index.row()][index.column()])
        return None

    def headerData(self, column, orientation, role):
        """Returns the header data

        :param column: The column
        :param orientation: The orientation
        :param role: The role
        """
        if self._is_data_set and orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self._data.columns[column]
        return None

    def getValue(self, index):
        """Returns the value of the given index

        :param index: The index
        """
        return self._data[index:index+1] if self._is_data_set else -1

    def _getColumnName(self, column):
        """Returns the column name, 'overrides' the properties column, takes 'id' instead

        :param column: The column ID
        """
        switcher = {
            0: 'active',
            1: 'authorities',
            2: 'created',
            3: 'id',
            4: 'modified',
            5: 'id',  # 'properties' cannot be sorted
            6: 'version'
        }
        return switcher.get(column, 'id')

    def filter(self, filter):
        """Filters the data

        :param filter: The filter string
        """
        if self._is_data_set:
            if not filter:
                logging.debug('Resetting filter')
                self.setData(self._original_data)
            else:
                mask_authorities = self._original_data['authorities'].astype(str).str.contains(
                    filter, na=False, flags=re.IGNORECASE)
                mask_id = self._original_data['id'].str.contains(
                    filter, na=False, flags=re.IGNORECASE)
                mask_properties = self._original_data['properties'].astype(str).str.contains(
                    filter, na=False, flags=re.IGNORECASE)
                self.setData(
                    self._original_data[mask_authorities | mask_id | mask_properties], update_orig_data=False)

    def sort(self, column, order):
        """Sort table by given column
        Columns:
        0: active | 1: authorities | 2: created | 3: id | 4: modified | 5: properties | 6: version

        :param column: The column
        :param order: The order
        """
        if self._is_data_set:
            ascending = order == Qt.AscendingOrder
            column_name = self._getColumnName(column)
            logging.debug(
                'Sorting column {} ("{}") by order {} ({})'.format(column, column_name, order, 'ascending' if ascending else 'descending'))
            self.setData(self._data.sort_values(
                by=[column_name], ascending=ascending, na_position='last'), False)
