#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2019 Denis Meyer
#
# This file is part of pycoyocloud.
#

"""Thread"""

import logging
import os
import json
import csv

from PyQt5.QtCore import QObject, QRunnable, pyqtSlot

from gui.signals.WorkerSignals import WorkerSignals


class DownloadThread(QRunnable):
    """Download thread"""

    def __init__(self, ops):
        """Initializes the thread

        :param ops: The operations
        """
        super().__init__()

        logging.debug('Initializing DownloadThread')

        self.ops = ops

        self.signals = WorkerSignals()

    @pyqtSlot()
    def run(self):
        """Runs the thread"""
        try:
            tenants = []
            from_cache = False
            req_getallusers = self.ops.get_tenants()
            if req_getallusers[0]:
                tenants = req_getallusers[1]
                from_cache = req_getallusers[2]
                logging.info('Downloaded successful. From cache: {}'.format(from_cache))
            else:
                logging.error('Could not download tenants.')
                raise Exception('Could not download tenants.')
        except Exception as ex:
            logging.exception('Failed to download')
            self.signals.downloaderror.emit(ex)
        else:
            self.signals.downloadresult.emit(tenants, from_cache)
        finally:
            self.signals.downloadfinished.emit()
