#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2019 Denis Meyer
#
# This file is part of pycoyocloud.
#

"""Thread"""

import logging
import os
import json
import csv

from PyQt5.QtCore import QObject, QRunnable, pyqtSlot

from gui.signals.WorkerSignals import WorkerSignals


class NewLicenseThread(QRunnable):
    """New license thread"""

    def __init__(self, ops, tenant, license):
        """Initializes the thread

        :param ops: The operations
        :param tenant: The tenant
        :param license: The license
        """
        super().__init__()

        logging.debug('Initializing NewLicenseThread')

        self.ops = ops
        self.tenant = tenant
        self.license = license

        self.signals = WorkerSignals()

    @pyqtSlot()
    def run(self):
        """Runs the thread"""
        try:
            req_newlicense = self.ops.set_license(self.tenant, self.license)
            if req_newlicense[0]:
                logging.info('New license set successfully.')
            else:
                logging.error('Could not set new license.')
                raise Exception('Could not set new license.')
        except Exception as ex:
            logging.exception('Failed to set new license.')
            self.signals.newlicenseerror.emit(ex)
        else:
            self.signals.newlicenseresult.emit()
        finally:
            self.signals.newlicensefinished.emit()
