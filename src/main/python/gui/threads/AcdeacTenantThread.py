#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2019 Denis Meyer
#
# This file is part of pycoyocloud.
#

"""Thread"""

import logging
import os
import json
import csv

from PyQt5.QtCore import QObject, QRunnable, pyqtSlot

from gui.signals.WorkerSignals import WorkerSignals


class AcdeacTenantThread(QRunnable):
    """Activate/Deactivate tenant thread"""

    def __init__(self, ops, tenant):
        """Initializes the thread

        :param ops: The operations
        :param tenant: The tenant
        """
        super().__init__()

        logging.debug('Initializing AcdeacTenantThread')

        self.ops = ops
        self.tenant = tenant

        self.signals = WorkerSignals()

    @pyqtSlot()
    def run(self):
        """Runs the thread"""
        try:
            if self.tenant.active:
                req_acdeactenant = self.ops.deactivate_tenant(self.tenant)
            else:
                req_acdeactenant = self.ops.activate_tenant(self.tenant)
            if req_acdeactenant[0]:
                logging.info('Activating/deactivating tenant successful.')
            else:
                logging.error('Could not activate/deactivate tenant.')
                raise Exception('Could not activate/deactivate tenants')
        except Exception as ex:
            logging.exception('Failed to activate/deactivate tenant')
            self.signals.acdeactenanterror.emit(ex)
        else:
            self.signals.acdeactenantresult.emit()
        finally:
            self.signals.acdeactenantfinished.emit()
