#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2019 Denis Meyer
#
# This file is part of pycoyocloud.
#

"""Thread"""

import logging
import os
import json
import csv

from PyQt5.QtCore import QObject, QRunnable, pyqtSlot

from gui.signals.WorkerSignals import WorkerSignals


class DeleteTenantThread(QRunnable):
    """Delete tenant thread"""

    def __init__(self, ops, tenant):
        """Initializes the thread

        :param ops: The operations
        :param tenant: The tenant
        """
        super().__init__()

        logging.debug('Initializing DeleteTenantThread')

        self.ops = ops
        self.tenant = tenant

        self.signals = WorkerSignals()

    @pyqtSlot()
    def run(self):
        """Runs the thread"""
        try:
            req_deletetenant = self.ops.delete_tenant(self.tenant)
            if req_deletetenant[0]:
                logging.info('Deleting tenant successful.')
            else:
                logging.error('Could not delete tenant.')
                raise Exception('Could not delete tenants')
        except Exception as ex:
            logging.exception('Failed to delete tenant')
            self.signals.deletetenanterror.emit(ex)
        else:
            self.signals.deletetenantresult.emit()
        finally:
            self.signals.deletetenantfinished.emit()
