#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2019 Denis Meyer
#
# This file is part of pycoyocloud.
#

"""Thread"""

import logging
import os
import json
import csv

from PyQt5.QtCore import QObject, QRunnable, pyqtSlot

from gui.signals.WorkerSignals import WorkerSignals


class CreateTenantThread(QRunnable):
    """Create tenant thread"""

    def __init__(self, ops, active, blank, url):
        """Initializes the thread

        :param ops: The operations
        :param active: Boolean flag
        :param blank: Boolean flag
        :param url: The URL
        """
        super().__init__()

        logging.debug('Initializing CreateTenantThread')

        self.ops = ops
        self.active = active
        self.blank = blank
        self.url = url

        self.signals = WorkerSignals()

    @pyqtSlot()
    def run(self):
        """Runs the thread"""
        try:
            req_createtenant = self.ops.create_tenant(
                self.active, self.blank, self.url)
            if req_createtenant[0]:
                logging.info('Creating tenant successful.')
            else:
                logging.error('Could not create tenant.')
                raise Exception('Could not create tenants')
        except Exception as ex:
            logging.exception('Failed to create tenant')
            self.signals.createtenanterror.emit(ex)
        else:
            self.signals.createtenantresult.emit()
        finally:
            self.signals.createtenantfinished.emit()
