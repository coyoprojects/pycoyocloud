#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2019 Denis Meyer
#
# This file is part of pycoyocloud.
#

"""Worker Signals"""

from PyQt5.QtCore import QObject, pyqtSignal


class WorkerSignals(QObject):
    """
    Defines the signals available from a running worker thread.

    Supported signals are:

    downloadfinished
        No data
    downloaderror
        `object` error data, anything
    downloadresult
        `object` data returned from processing, anything

    deletetenantfinished
        No data
    deletetenanterror
        `object` error data, anything
    deletetenantresult
        `object` data returned from processing, anything

    acdeactenantfinished
        No data
    acdeactenanterror
        `object` error data, anything
    acdeactenantresult
        `object` data returned from processing, anything

    createtenantfinished
        No data
    createtenanterror
        `object` error data, anything
    createtenantresult
        `object` data returned from processing, anything

    newlicensefinished
        No data
    newlicenseerror
        `object` error data, anything
    newlicenseresult
        `object` data returned from processing, anything
    """
    downloadfinished = pyqtSignal()
    downloaderror = pyqtSignal(object)
    downloadresult = pyqtSignal(object, object)
    deletetenantfinished = pyqtSignal()
    deletetenanterror = pyqtSignal(object)
    deletetenantresult = pyqtSignal()
    acdeactenantfinished = pyqtSignal()
    acdeactenanterror = pyqtSignal(object)
    acdeactenantresult = pyqtSignal()
    createtenantfinished = pyqtSignal()
    createtenanterror = pyqtSignal(object)
    createtenantresult = pyqtSignal()
    newlicensefinished = pyqtSignal()
    newlicenseerror = pyqtSignal(object)
    newlicenseresult = pyqtSignal()
