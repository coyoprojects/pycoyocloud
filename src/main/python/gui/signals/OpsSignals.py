#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2019 Denis Meyer
#
# This file is part of pycoyocloud.
#

"""Ops Signals"""

from PyQt5.QtCore import QObject, pyqtSignal


class OpsSignals(QObject):
    """
    Defines the signals available from operations.

    Supported signals are:

    fetchedtenants
    deletedtenant
    acdeactenant
    newlicense
    createdtenant
    """
    fetchedtenants = pyqtSignal()
    deletedtenant = pyqtSignal()
    acdeactenant = pyqtSignal()
    newlicense = pyqtSignal()
    createdtenant = pyqtSignal()
