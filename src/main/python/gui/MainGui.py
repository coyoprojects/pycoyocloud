#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2019 Denis Meyer
#
# This file is part of pycoyocloud.
#

"""GUI"""

import logging
import sys

from lib.coyo.CoyoData import CoyoData
from gui.components.AppContext import AppContext


class GUI():
    """Main GUI"""

    def __init__(self, coyodata):
        """Initializes the GUI

        :param coyodata: The CoyoData
        """
        logging.debug('Initializing MainGUI')

        appctxt = AppContext(coyodata)
        exit_code = appctxt.run()
        sys.exit(exit_code)
