#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2019 Denis Meyer
#
# This file is part of pycoyocloud.
#

"""COYO Tenant"""

import logging


class CoyoTenant:
    """A COYO tenant"""

    def __init__(self, df=None):
        """Initialization

        :param df: The Dataframe
        """
        self.df = df

        self.deleted = False

        self.active = False
        self.authorities = []
        self.created = -1
        self.id = ''
        self.modified = -1
        self.properties = {
            'url': '',
            'license': ''
        }
        self.version = -1

        self._parse()

    def hasUrl(self):
        """Returns whether this tenant has a URL"""
        return self.properties['url'] != ''

    def getUrl(self):
        return self.properties['url']

    def hasLicense(self):
        """Returns whether this tenant has a license"""
        return self.properties['license'] != ''

    def _parse(self):
        """Extracts data from the Dataframe"""
        self.active = self.df['active'].tolist()[0]
        self.authorities = self.df['authorities'].tolist()[0]
        self.created = self.df['created'].tolist()[0]
        self.id = self.df['id'].tolist()[0]
        self.modified = self.df['modified'].tolist()[0]
        props = self.df['properties'].tolist()[0]
        self.properties = {
            'url': props['url'] if 'url' in props else '',
            'license': props['license'] if 'license' in props else '',
        }
        self.version = self.df['version'].tolist()[0]
