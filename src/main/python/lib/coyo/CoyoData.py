#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2019 Denis Meyer
#
# This file is part of pycoyocloud.
#

"""COYO Data"""


class CoyoData:
    """Stores COYO data"""

    def __init__(self, baseurl='', username='', password=''):
        """Initialization

        :param baseurl: COYO Cloud base URL
        :param username: COYO username
        :param password: COYO password
        """
        self.baseurl = baseurl if not baseurl.endswith('/') else baseurl[:-1]
        self.username = username
        self.password = password

    def extract_protocol(self):
        """Extracts the protocol"""
        if self.baseurl.startswith('http:'):
            return 'http'

        return 'https'

    def extract_base_url(self):
        """Extracts the base URL"""
        base_url = self.baseurl
        if self.baseurl.startswith('https://'):
            base_url = base_url[8:]
        if self.baseurl.startswith('http://'):
            base_url = base_url[7:]
        if self.baseurl.startswith('www.'):
            base_url = base_url[4:]

        return base_url
