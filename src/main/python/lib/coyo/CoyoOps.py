#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2019 Denis Meyer
#
# This file is part of pycoyocloud.
#

"""COYO Operations"""

import logging
import json
import base64
import re
import os
from pathlib import Path
from urllib.request import Request, urlopen
from urllib.parse import urlencode
from urllib.request import HTTPPasswordMgrWithDefaultRealm, HTTPBasicAuthHandler, build_opener, install_opener

import pandas as pd

from lib.AppConfig import app_conf_get
from gui.signals.OpsSignals import OpsSignals


def clear_tenant_cache():
    """Deletes the tenant cache"""
    try:
        logging.debug(
            'Trying to delete file "{}"'.format(app_conf_get('cache_file')))
        os.remove(app_conf_get('cache_file'))
        logging.debug('File successfully deleted')
    except OSError:
        logging.debug('File does not exist')


class CoyoOps:
    """Authenticates with COYO and performs requests

    Tenant documentation: https://www.coyocloud.com/docs/guide/operations/manage/manage:tenants
    """

    _urlpaths = {
        '/manage/tenants': '/manage/tenants',
        '/activate': '/activate',
        '/deactivate': '/deactivate',
        '/license': '/license'
    }

    def __init__(self, coyodata):
        """Initialization

        :param coyodata: The CoyoData
        """
        self.coyodata = coyodata
        self.signals = OpsSignals()
        self._prepare_password_mgr()

    def _prepare_password_mgr(self):
        """Prepares the password manager"""
        logging.debug('Preparing password manager')
        password_mgr = HTTPPasswordMgrWithDefaultRealm()
        password_mgr.add_password(
            None, self.coyodata.baseurl, self.coyodata.username, self.coyodata.password)
        handler = HTTPBasicAuthHandler(password_mgr)
        opener = build_opener(handler)
        opener.open(self.coyodata.baseurl)
        install_opener(opener)

    def _load_from_cache(self):
        """Loads the JSON data from cache"""
        logging.debug('Loading data from cache')
        return pd.read_json(app_conf_get('cache_file'))

    def _write_to_cache(self, df_data):
        """Writes JSON data to cache

        :param df_data: Dataframe with JSON data to write"""
        logging.debug('Writing to cache')
        df_data.to_json(app_conf_get('cache_file'), orient='records')

    def _load_live(self):
        """Loads the live JSON data"""
        logging.debug('Loading live data')
        url = self.coyodata.baseurl + self._urlpaths['/manage/tenants']
        logging.debug('GET "{}"'.format(url))
        df_data = pd.read_json(url, orient='columns')
        self._write_to_cache(df_data)
        return df_data

    def get_tenants(self):
        """Retrieves the tenants and returns a dataframe"""
        # self.signals.fetchedtenants.emit()
        found = False
        data = None
        from_cache = False
        try:
            data = self._load_from_cache()
            from_cache = True
            found = True
        except Exception:
            logging.debug('Cache not filled')
        if not found:
            try:
                data = self._load_live()
                found = True
            except Exception:
                logging.error('Error loading data')
        return (found, data, from_cache)

    def delete_tenant(self, tenant):
        """Deletes a tenant

        :param tenant: The tenant
        """
        cloud_url = self.coyodata.baseurl + \
            self._urlpaths['/manage/tenants'] + '/' + tenant.id
        cloud_url_deactivate = cloud_url + self._urlpaths['/deactivate']
        logging.debug('PUT "{}"'.format(cloud_url_deactivate))
        req_deactivate = Request(cloud_url_deactivate, method='PUT')
        with urlopen(req_deactivate):
            logging.debug('DELETE "{}"'.format(cloud_url))
            req = Request(cloud_url, method='DELETE')
            with urlopen(req) as request:
                return (True, request.read().decode('utf-8'))

    def activate_tenant(self, tenant):
        """Activates a tenant

        :param tenant: The tenant
        """
        cloud_url = self.coyodata.baseurl + \
            self._urlpaths['/manage/tenants'] + '/' + tenant.id
        cloud_url_activate = cloud_url + self._urlpaths['/activate']
        logging.debug('PUT "{}"'.format(cloud_url_activate))
        req_activate = Request(cloud_url_activate, method='PUT')
        with urlopen(req_activate) as request:
            return (True, request.read().decode('utf-8'))

    def deactivate_tenant(self, tenant):
        """Deactivates a tenant

        :param tenant: The tenant
        """
        cloud_url = self.coyodata.baseurl + \
            self._urlpaths['/manage/tenants'] + '/' + tenant.id
        cloud_url_deactivate = cloud_url + self._urlpaths['/deactivate']
        logging.debug('PUT "{}"'.format(cloud_url_deactivate))
        req_deactivate = Request(cloud_url_deactivate, method='PUT')
        with urlopen(req_deactivate) as request:
            return (True, request.read().decode('utf-8'))

    def set_license(self, tenant, license):
        """Deactivates a tenant

        :param tenant: The tenant
        :param license: The license
        """
        data = {
            'license': license
        }
        json_data = json.dumps(data, ensure_ascii=False)
        json_data_bytes = json_data.encode('utf-8')
        cloud_url = self.coyodata.baseurl + \
            self._urlpaths['/manage/tenants'] + '/' + tenant.id
        cloud_url = cloud_url + self._urlpaths['/license']
        logging.debug('PUT "{}" with data "{}"'.format(
            cloud_url, json_data))
        req = Request(cloud_url, method='PUT')
        req.add_header('Content-Type', 'application/json; charset=utf-8')
        req.add_header('Content-Length', len(json_data_bytes))
        with urlopen(req, json_data_bytes) as request:
            return (True, request.read().decode('utf-8'))

    def create_tenant(self, active, blank, url):
        """Creates a tenant

        :param active: Boolean flag
        :param blank: Boolean flag
        :param url: The URL
        """
        lst = [url]
        data = {
            'active': active,
            'blank': blank,
            'urls': lst
        }
        json_data = json.dumps(data, ensure_ascii=False)
        json_data_bytes = json_data.encode('utf-8')
        cloud_url = self.coyodata.baseurl + self._urlpaths['/manage/tenants']
        logging.debug('POST "{}" with data "{}"'.format(
            cloud_url, json_data))
        req = Request(cloud_url, method='POST')
        req.add_header('Content-Type', 'application/json; charset=utf-8')
        req.add_header('Content-Length', len(json_data_bytes))
        with urlopen(req, json_data_bytes) as request:
            return (True, request.read().decode('utf-8'))
