#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2019 Denis Meyer
#
# This file is part of pycoyocloud.
#

"""Main"""

import os
import logging
import urllib.request
import json

import pandas as pd

from lib.AppConfig import app_conf_get
from lib.coyo.CoyoData import CoyoData
from gui.MainGui import GUI


def _initialize_logger():
    """Initializes the logger"""
    if app_conf_get('logging.log_to_file'):
        basedir = os.path.dirname(app_conf_get('logging.logfile'))

        if not os.path.exists(basedir):
            os.makedirs(basedir)

    logging.basicConfig(level=app_conf_get('logging.loglevel'),
                        format=app_conf_get('logging.format'),
                        datefmt=app_conf_get('logging.datefmt'))

    if app_conf_get('logging.log_to_file'):
        handler_file = logging.FileHandler(
            app_conf_get('logging.logfile'), mode='w', encoding=None, delay=False)
        handler_file.setLevel(app_conf_get('logging.loglevel'))
        handler_file.setFormatter(logging.Formatter(
            fmt=app_conf_get('logging.format'), datefmt=app_conf_get('logging.datefmt')))
        logging.getLogger().addHandler(handler_file)


def _load_settings(settings_file):
    """Loads the settings"""
    baseurl = ''
    username = ''
    password = ''
    try:
        logging.debug('Trying to load settings file')
        with open(settings_file) as f:
            settings_json = json.load(f)
        if 'BASE_URL' in settings_json:
            baseurl = settings_json['BASE_URL']
            logging.debug('Parsed "BASE_URL"')
        if 'USERNAME' in settings_json:
            username = settings_json['USERNAME']
            logging.debug('Parsed "USERNAME"')
        if 'PASSWORD' in settings_json:
            password = settings_json['PASSWORD']
            logging.debug('Parsed "PASSWORD"')
        logging.debug('Successfully loaded settings file')
    except Exception as e:
        logging.debug('Could not load settings file: "{}"'.format(e))

    return (baseurl, username, password)


if __name__ == '__main__':
    _initialize_logger()
    baseurl, username, password = _load_settings(app_conf_get('settings_file'))

    coyodata = CoyoData(baseurl=baseurl, username=username, password=password)
    gui = GUI(coyodata)
